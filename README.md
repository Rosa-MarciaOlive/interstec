<img src='Captura_de_tela_2021-06-04_191453.png' width = 920 height = 450>
<br>


### Integrantes do grupo: 
- [Rosa Márcia](https://www.gitlab.com/Rosa-MarciaOlive)
- [Rafael](https://www.gitlab.com/Rapharlb)
- [Samuel](https://www.gitlab.com/samuemalves)


### Introdução do projeto:
- O InterestTEC tem como objetivo através do web bot a automação de página, filtrar informação sobre tecnologia e compartilha-las.
- Segue o link do vídeo da apresentação do nosso projeto!-> [clik aqui!!](https://www.youtube.com/watch?v=ii669V6PtF8)
- Siga nossa pagina no Twitter o [interest_tec.](https://twitter.com )


### Big Data no Agronegócio:
- [FATEC Pompeia SP](https://www.vestibularfatec.com.br/unidades-cursos/escola.asp?c=240)
- [Grupo BDAG no Git Lab](https://gitlab.com/BDAg/intersTec)
<br>
